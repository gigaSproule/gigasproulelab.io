#!/bin/bash

cd themes/icarus
git init
git remote add icarus git@github.com:ppoffice/hexo-theme-icarus.git
git fetch --prune icarus
rm -rf .github .gitignore includes languages layout LICENSE package.json README.md scripts source
git checkout -b master icarus/master
git pull
rm -rf .git .github .gitignore README.md LICENSE

cd ../..
